Участники проекта MarketApp

- Сочнев Роман
- Крупенко Николай
- Коняев Владислав
- Лепёшкин Кирилл

ProductInfo

String id
String name
String composition
double price;
String categoryId;
double weight;

CategoryInfo

String id
String name

ТЗ

1. MarketController path="/market".

@GetMapping
ModelAndView getAll() {...}

@GetMapping("/item")
ModelAndView getProduct(@RequestParam("itemId") String itemId) {...}

@GetMapping("/item/create")
ModelAndView getCreateProductView() {...}

@PostMapping("/item/create")
ModelAndView createProduct(@RequestBody ProductInfo info) {...}

2. ProductRestController path="/api/product"

@GetMapping("/all")
List<ProductInfo> getAll() {...}

@GetMapping
ProductInfo getProduct(@RequestParam("itemId") String itemId)

@PostMapping("/create")
ProductInfo create(@RequestBody ProductInfo info)

@PostMapping("/update")
ProductInfo update(@RequestBody ProductInfo info)

@PostMapping("/delete")
ProductInfo delete(@RequestParam("itemId") String itemId)

3. ProductService (бизнес логика)

List<ProductInfo> getAll() {...}

ProductInfo getProduct(String itemId)

ProductInfo create(ProductInfo info)

ProductInfo update(ProductInfo info)

ProductInfo delete(String itemId)

Map<String, CategoryInfo> getCategories() // constants

*** List<ProductInfo> getSimilarProducts(String itemId)


4. ProductRepository (Работа с БД)

List<ProductInfo> getAll() {...}

ProductInfo getProduct(String itemId)

ProductInfo create(ProductInfo info)

ProductInfo update(ProductInfo info)

ProductInfo delete(String itemId)

Критерии приемки

1. Оценка 3 - Spring MVC or SpringBoot + JSP

- Реализованы классы ProductRepository + ProductService + MarketController (без учета ***)

- Тестовые значения хранятся в БД. Значения можно менять только в БД

2. Оценка 4 - ProductRestController

Значения можно менять через REST (Postman, SoapUI, ...)

3. Оценка 5 - Страницы MarketApp содержат дополнительную информацию

- Раздел "Смотрите также" с информацию о других продуктах данной категории с ограничением по цене (+- 200 рублей - параметр в настройках приложения)
